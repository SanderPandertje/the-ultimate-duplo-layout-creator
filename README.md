# StakeLogic Duplo visual layout builder

---

##### The Ultimate Duplo Layout Creator

## Install

Installation of dependencies:

```bash
npm install
```

**Having issues installing? See the
electron-react [debugging guide](https://github.com/electron-react-boilerplate/electron-react-boilerplate/issues/400)**

## Starting Development

Start the app in the `dev` environment:

```bash
npm start
```

## Packaging for Production

To package apps for the local platform:

```bash
npm run package
```

## Docs

See
the `Ultimate Duplo Layout Creator's` [docs here](https://bitbucket.org/SanderPandertje/the-ultimate-duplo-layout-creator/src/master/README.md)

See the electron-react [docs and guides here](https://electron-react-boilerplate.js.org/docs/installation)
