import { styled } from '@mui/material';
import { FC } from 'react';
import { ThemeColors } from '../../../../renderer/styles/theme';

type ItemPropertyManagerProps = {
  children?: any[];
};

const ItemPropertyMenu = styled('div')(() => ({
  backgroundColor: ThemeColors.spaceCadetBlue.main,
}));

const ItemPropertyManager: FC<ItemPropertyManagerProps> = () => {
  return <ItemPropertyMenu></ItemPropertyMenu>;
};

export default ItemPropertyManager;
