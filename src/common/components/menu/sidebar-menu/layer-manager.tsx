import { styled } from '@mui/material';
import { FC } from 'react';
import { ThemeColors } from '../../../../renderer/styles/theme';

type LayerManagerProps = {
  children?: any[];
};

const LayerManagerMenu = styled('div')(() => ({
  backgroundColor: ThemeColors.spaceCadetBlue.main,
}));

const LayerManager: FC<LayerManagerProps> = () => {
  return <LayerManagerMenu></LayerManagerMenu>;
};

export default LayerManager;
