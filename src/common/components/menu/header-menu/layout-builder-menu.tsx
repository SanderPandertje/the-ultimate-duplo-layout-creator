import { styled } from '@mui/material';
import slLogo from 'common/assets/Stakelogic Logo Right Alligned Light.svg';
import { FC } from 'react';

import { spaceCadetBlue, ThemeColors } from '../../../../renderer/styles/theme';
import MenuItemHolder from './menu-item-holder';

export const menuBarHeight = 4;

const Header = styled('div')(() => ({
  display: 'flex',
  gap: '1rem',
  color: ThemeColors.white,
  padding: '0 0.5rem',
  justifyContent: 'space-between',
  alignItems: 'center',
  width: 'calc(100%-2*0.5rem)',
  backgroundColor: spaceCadetBlue,
}));

const SLLogoImage = styled('img')(() => ({
  height: `${menuBarHeight * 0.65}rem`,
}));

const LayoutBuilderMenu: FC = () => {
  return (
    <>
      <Header>
        <MenuItemHolder />
        <SLLogoImage src={slLogo} alt="" />
      </Header>
    </>
  );
};
export default LayoutBuilderMenu;
