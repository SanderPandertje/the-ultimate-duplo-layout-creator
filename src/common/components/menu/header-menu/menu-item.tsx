import { styled, SvgIcon, Typography } from '@mui/material';
import { ElementType, FC } from 'react';
import { ThemeColors } from '../../../../renderer/styles/theme';
import { menuBarHeight } from './layout-builder-menu';

const Item = styled('div')(() => ({
  classname: 'menu-item',
  display: 'grid',
  placeItems: 'center',
  height: `calc(${menuBarHeight * 0.8}rem - 0.3rem)`,
  padding: '0.2rem 0.7rem',
  backgroundColor: ThemeColors.spaceCadetBlue.dark,
  borderRadius: '0.2rem',
}));

const Contents = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyItems: 'center',
  gap: '0.2rem',
}));

type MenuItemProps = {
  text: string;
  icon?: ElementType;
};

const MenuItem: FC<MenuItemProps> = ({ text, icon }) => {
  return (
    <Item>
      <Contents>
        {icon && <SvgIcon component={icon} />}
        <Typography variant={'body2'}>{text}</Typography>
      </Contents>
    </Item>
  );
};

export default MenuItem;
