import AddIcon from '@mui/icons-material/Add';
import { styled } from '@mui/material';
import { FC } from 'react';
import { menuBarHeight } from './layout-builder-menu';
import MenuItem from './menu-item';

const Flex = styled('div')(() => ({
  position: 'relative',
  display: 'flex',
  alignItems: 'center',
  flexDirection: 'row',
  height: `${menuBarHeight}rem`,
  width: '100%',
  gap: '0.25rem',
}));

const MenuItemHolder: FC = () => {
  return (
    <Flex>
      <MenuItem text={'icon 1'} icon={AddIcon} />
      <MenuItem text={'icon 2'} />
      <MenuItem text={'icon 3'} icon={AddIcon} />
      <MenuItem text={'icon 4'} icon={AddIcon} />
    </Flex>
  );
};

export default MenuItemHolder;
