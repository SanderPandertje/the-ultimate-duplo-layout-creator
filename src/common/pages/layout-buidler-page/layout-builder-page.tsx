import { styled } from '@mui/material';
import { FC } from 'react';

import { ThemeColors } from '../../../renderer/styles/theme';
import LayoutBuilderMenu, {
  menuBarHeight,
} from '../../components/menu/header-menu/layout-builder-menu';
import ItemPropertyManager from '../../components/menu/sidebar-menu/item-property-manager';
import LayerManager from '../../components/menu/sidebar-menu/layer-manager';

const Body = styled('div')(() => ({
  display: 'grid',
  height: `calc(100vh - ${menuBarHeight}rem)`,
  position: 'relative',
  width: '100%',
  gridTemplateColumns: '2fr 8fr 3fr',
  gridTemplateRows: '1fr',
  backgroundColor: ThemeColors.spaceCadetBlue.light,
}));

const LayoutBuilderPage: FC = (): JSX.Element => (
  <div>
    <LayoutBuilderMenu />
    <Body>
      <ItemPropertyManager />
      <div />
      <LayerManager />
    </Body>
  </div>
);

export default LayoutBuilderPage;
