import { createTheme } from '@mui/material';

export const ThemeColors = {
  malachiteGreen: {
    main: '#00d45c',
    light: '#61ff8c',
    dark: '#00a12d',
    contrastText: '#000000',
    50: '#e4f9e9',
    100: '#bff0c8',
    200: '#92e6a3',
    300: '#5add7c',
    400: '#00d45c',
    500: '#00cb39',
    600: '#00bb2f',
    700: '#00a720',
    800: '#00960e',
    900: '#007500',
  },
  spaceCadetBlue: {
    main: '#161b3b',
    light: '#404166',
    dark: '#000017',
    contrastText: '#ffffff',
    50: '#e5e7ed',
    100: '#bec3d3',
    200: '#959db5',
    300: '#6d7897',
    400: '#515c83',
    500: '#354272',
    600: '#2F3C6A',
    700: '#27335F',
    800: '#202A53',
    900: '#161b3b',
  },
  white: '#FFFFFF',
};

export const malachiteGreen = ThemeColors.malachiteGreen.main;
export const spaceCadetBlue = ThemeColors.spaceCadetBlue.main;

const Theme = createTheme({
  palette: {
    primary: ThemeColors.malachiteGreen,
    secondary: ThemeColors.spaceCadetBlue,
  },
  typography: {
    fontFamily: 'Helvetica Neue',
    fontSize: 13,
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        html: {
          width: '100vw',
          overflowX: 'hidden',
          height: '100%',
        },
      },
    },
  },
});

export default Theme;
