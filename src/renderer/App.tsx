import { ThemeProvider } from '@mui/material';
import { MemoryRouter as Router, Route, Routes } from 'react-router-dom';
import './app.scss';
import LayoutBuilderPage from '../common/pages/layout-buidler-page/layout-builder-page';
import Theme from './styles/theme';

export default function App() {
  return (
    <ThemeProvider theme={Theme}>
      <Router>
        <Routes>
          <Route path="/" element={<LayoutBuilderPage />} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}
